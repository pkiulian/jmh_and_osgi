package test.java;

import java.math.BigInteger;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.Test;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import array.processor.api.IByteArrayProcessor;
import array.processor.internal.impl.ByteArrayProcessor;


public class TestPerformance {

	static IByteArrayProcessor processor = new ByteArrayProcessor();

	@State(Scope.Benchmark)
	public static class BenchmarkState {

		@Param({ "10", "100", "1000" })
		public int inputArraySizes;

		public byte[] inputBytes;

		public String inputString;

		@Setup(Level.Trial)
		public void setUp() {

			inputString = new Random().ints(1, 100).limit(inputArraySizes).boxed().map(e -> e % 2).map(String::valueOf)
					.collect(Collectors.joining());

			System.out.println("Generated input string: " + inputString);

			// from the 0 and 1 string to byte array.
			inputBytes = new BigInteger(inputString, 2).toByteArray();
		}
	}

	@Benchmark
	@BenchmarkMode(Mode.AverageTime)
	@OutputTimeUnit(TimeUnit.MICROSECONDS)
	@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
	@Measurement(iterations = 10, time = 2, timeUnit = TimeUnit.SECONDS)
	public void measureAvgTimeMyMethodWithNoise(BenchmarkState state) throws InterruptedException {
		TestPerformance.processor.processToInteger(state.inputBytes);
	}

	@Benchmark
	@BenchmarkMode(Mode.AverageTime)
	@OutputTimeUnit(TimeUnit.MICROSECONDS)
	@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
	@Measurement(iterations = 10, time = 2, timeUnit = TimeUnit.SECONDS)
	public void measureAvgTimeDefaultWithNoise(BenchmarkState state) throws InterruptedException {
		TestPerformance.processor.processToIntegerDefault(state.inputBytes);
	}

	// ***********************************************************************************************************************

	@Benchmark
	@BenchmarkMode(Mode.AverageTime)
	@OutputTimeUnit(TimeUnit.MICROSECONDS)
	@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
	@Measurement(iterations = 10, time = 2, timeUnit = TimeUnit.SECONDS)
	public void measureAvgTimeMyMethodWithoutNoise(BenchmarkState state) throws InterruptedException {
		TestPerformance.processor.processToInteger(state.inputString);
	}

	@Benchmark
	@BenchmarkMode(Mode.AverageTime)
	@OutputTimeUnit(TimeUnit.MICROSECONDS)
	@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
	@Measurement(iterations = 10, time = 2, timeUnit = TimeUnit.SECONDS)
	public void measureAvgTimeDefaultWithoutNoise(BenchmarkState state) throws InterruptedException {
		TestPerformance.processor.processToIntegerDefault(state.inputString);
	}

	@Test
	public void testPerformance() throws RunnerException {

		Options opt = new OptionsBuilder().include(TestPerformance.class.getSimpleName())
				.resultFormat(ResultFormatType.JSON).result("results_tests/" + System.currentTimeMillis() + ".json")
				.forks(0).build();
		new Runner(opt).run();
	}

}

package array.processor.user;

import java.math.BigInteger;
import java.security.SecureRandom;

import array.processor.api.IByteArrayProcessor;

public class Client {
	
	IByteArrayProcessor service;
	
	public void setService(IByteArrayProcessor service) {
		this.service = service;
	}
	
	
	public void startUp() {
		
		 
		 byte[] inputBytes = new byte[1];			
		 
		 SecureRandom random = new SecureRandom();
		 random.nextBytes(inputBytes);	 
		 random.nextBytes(inputBytes);	 
		 random.nextBytes(inputBytes);	 
		
		
		 BigInteger result = this.service.processToInteger(inputBytes);
         System.out.println("my      result "+result);
         
      
         BigInteger resultDefault = this.service.processToIntegerDefault(inputBytes);
         System.out.println("default result "+resultDefault);
	}
}

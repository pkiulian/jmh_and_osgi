package array.processor.internal.impl;

import java.math.BigInteger;

import array.processor.api.IByteArrayProcessor;

public class ByteArrayProcessor implements IByteArrayProcessor {

	@Override
	public BigInteger processToInteger(byte[] binaryByte) {
		String inputString = calculateInputString(binaryByte); // this is useless noise - visible through VisualVM
		BigInteger result = calculateNumberResult(inputString);
		return result;
	}

	@Override
	public BigInteger processToIntegerDefault(byte[] binaryByte) {
		String inputString = calculateInputString(binaryByte); // this is useless noise - visible through VisualVM
		return new BigInteger(inputString);
	}

	@Override
	public BigInteger processToIntegerDefault(String stringInput) {
		return new BigInteger(stringInput);
	}

	@Override
	public BigInteger processToInteger(String inputString) {
		return calculateNumberResult(inputString);
	}

	private BigInteger calculateNumberResult(String inputString) {
		BigInteger two = new BigInteger("2");
		BigInteger result = new BigInteger("0");

		int power = 0;
		for (int i = inputString.length() - 1; i >= 0; i--) {
			if (inputString.charAt(i) == '1') {
				BigInteger partial = two.pow(power);
				result = result.add(partial);
			}
			power++;
		}
		return result;
	}

	private String calculateInputString(byte[] binaryByte) {
		String result = "";
		for (int octet = 0; octet < binaryByte.length; octet++) {
			result += String.format("%8s", Integer.toBinaryString(octet & 0xFF)).replace(' ', '0');
		}
		return result;
	}

}

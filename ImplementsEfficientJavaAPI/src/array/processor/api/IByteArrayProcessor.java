package array.processor.api;

import java.math.BigInteger;

public interface IByteArrayProcessor {
	
	/** 
	 * Process the input byte array into an Integer by mathematical pow method + some extra noise 
	 * @param binaryByte
	 * @return
	 */
	BigInteger processToInteger(byte[] binaryByte);
	
	/**
	 * Process the input byte array into an Integer by bit shifting + some extra noise
	 * @param binaryByte
	 * @return
	 */
	BigInteger processToIntegerDefault(byte[] binaryByte);
	
	/**
	 * Process the input byte array into an Integer by bit shifting 
	 * @param stringInput
	 * @return
	 */
	BigInteger processToIntegerDefault(String stringInput);

	/**
	 * Process the input byte array into an Integer by mathematical pow method 
	 * 
	 * @param inputString
	 * @return
	 */
	BigInteger processToInteger(String inputString);

}
